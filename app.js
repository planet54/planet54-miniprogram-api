require('dotenv').config()

const express = require('express')
const app = express()
const port = (process.env.NODE_ENV === 'development') ? 3000 : process.env.PORT;
const bodyParser = require('body-parser')

const Shopify = require('shopify-api-node')
const parsePhoneNumber = require('libphonenumber-js')
const firebase = require('firebase/app').default

require('firebase/database')

const alipayClient = require('./services/alipay-client-service')

const shopifyConfig = require('./config/shopify-config')
const shopify = new Shopify(shopifyConfig)

const fireBaseConfig = require('./config/firebase-config')

const fbApp = firebase.initializeApp(fireBaseConfig);

app.use(bodyParser.json())

app.get('/', (req, res) => {
	res.send('Planet54 <> Vodacom Miniprogram')
})

app.get('/apply', async (req, res) => {

	const applyResponse = await alipayClient.applyAuthCode().then(res => res).catch(err => err)

	const { authCode, merchantId } = applyResponse

	res.send({
		authCode: authCode,
		merchantId: merchantId,
	})
})

app.post('/token', async (req, res) => {
	const authCode = req.body.authCode

	if (!authCode){
		res.send({ errors: { message: "Authcode missing" } })
		return;
	}

	const accessTokenResponse = await alipayClient.getAccessToken(authCode).then(res => res.data).catch(err => err)
	
	if (accessTokenResponse.result.resultStatus === 'F') {
		res.status(401)
		res.send({ error: { message: accessTokenResponse.result.resultMessage } })
		return
	}

	res.send({
		accessToken: accessTokenResponse.accessToken,
		accessTokenExpiryTime: accessTokenResponse.accessTokenExpiryTime,
		customerId: accessTokenResponse.customerId
	})
})

queryShopifyCustomerById = async (id) => {
	try {
		return await shopify.customer.get(id)
	} catch (error) {
		return error
	}
}

queryShopifyCustomer = async (email) => {
	try {
		return await shopify.customer.search({ query: email })
	} catch (error) {
		return error.response.body
	}
}

createShopifyCustomer = async (user) => {
	try {
		user.tags = 'Vodapay'
		const userCreate = await shopify.customer.create(user)
		return userCreate
	} catch (error) {
		return error.response.body
	}
}

getShopifyOrder = async (id) => {
	try {
		return await shopify.order.get(id)
	} catch (error) {
		return error
	}
}

app.get('/user/:id?', async (req, res) => {
	if (req.params.id) {
		const customer = await queryShopifyCustomerById(req.params.id)
		res.send(customer)
		return
	}

	const email = req.query.email, first_name = req.query.first_name, last_name = req.query.last_name
	let phone = req.query.phone

	if (!email) res.send({ error: { message: "email is invalid." } })

	if (phone) {
		const phoneNumber = parsePhoneNumber(phone, 'ZA')

		if (phoneNumber && phoneNumber.isValid()) {
			phone = phoneNumber.number
		}
	}

	const user = {
		email,
		phone
	}

	if (first_name) {
		user.first_name = first_name
	}

	if (last_name) {
		user.last_name = last_name
	}

	let response = await queryShopifyCustomer(user.email)

	if (response.length) {
		response = response[0]
	} else {
		response = await createShopifyCustomer(user)
	}

	res.send(response)

})

getCustomerOrder = async (id) => {
	try {
		return await shopify.customer.orders(id)
	} catch (error) {
		return error
	}
}

app.get('/users/:id/orders', async (req, res) => {
	const orders = await getCustomerOrder(req.params.id)
	res.send(orders)
})

app.post('/user', async (req, res) => {
	const accessToken = req.body.accessToken
	const customerId = req.body.customerId

	if (!accessToken) {
		res.send({ errors: { message: "Access token is invalid." } })
		return
	}

	if (!customerId) {
		res.send({ errors: { message: "Customer ID is invalid." } })
		return
	}

	console.log('accessToken =>', accessToken);
	console.log('customerId =>', customerId);

	let response = {}

	try {
	
		const userInfoResponse = await alipayClient.inquiryUserInfo(accessToken).then(res => res).catch(err => err)

		if (userInfoResponse.result.resultCode === 'SUCCESS') {
			response = {
				customer: userInfoResponse.userInfo
			};
		} else {
			res.status(401)
			response = userInfoResponse.result
		}

	} catch (error) {

		response = { errors: JSON.parse(error.response.config.data) }

	}

	res.send(response)
})

updateCustomerAddress = async (id, address) => {
	try {
		const customer = await shopify.customerAddress.create(id, address)
		return customer
	} catch (error) {
		return error.response.body
	}
}

app.post('/user/address/update', async (req, res) => {
	const customerId = req.body.id
	const address = req.body.address

	if (!customerId) {
		res.status(402)
		res.send({ error: { message: "customerId is missing" } })
		return
	}

	if (!address) {
		res.status(402)
		res.send({ error: { message: "Address field missing" } })
		return
	}

	const response = await updateCustomerAddress(customerId, address)

	res.send(response)

})

app.post('/pay', async (req, res) => {
	const body = req.body

	if (!body) {
		res.status(402)
		res.send({ error: { message: "request is invalid." } })
	}

	const payResponse = await alipayClient.pay(body).then(res => res).catch(err => err)

	res.send(payResponse)
})

createOrder = async (order) => {
	try {
		const orderCreate = await shopify.order.create(order)
		return orderCreate
	} catch (error) {
		console.error('error create order', error)
		return error.response.body
	}
}

app.get('/order/:id', async (req, res) => {
	const order = await getShopifyOrder(req.params.id)
	res.send(order)
})

app.post('/order/create', async (req, res) => {

	const body = req.body
	if (!body) {
		res.status(402)
		res.send({ error: { message: "Request is invalid." } })
	}

	body.tags = 'Vodapay'

	const orderRes = await createOrder(body)

	res.send(orderRes)

})

app.get('/orders/status', async (req, res) => {
	const body = req.body

	if (!body.paymentId) {
		res.status(400)
		res.send({
			errors: {
				code: 400,
				message: 'paymentId is required'
			}
		})
		return
	}
	
	const paymentRefResult = await fbApp.database().ref(`orders_refs/${body.paymentId}`).get()

	if (!paymentRefResult.val()) {
		res.status(404)
		res.send({
			errors: {
				code: 404,
				message: 'Order status not found'
			}
		})
		return
	}

	res.send(paymentRefResult)

})

app.post('/orders/status', async (req, res) => {
	const body = req.body
	if (!body.paymentId && !body.paymentRequestId) {
		res.status(400)
		res.send({
			errors: {
				code: 400,
				message: 'Order status unavailable'
			}
		})
		return
	}

  fbApp.database().ref(`orders_refs/${body.paymentId}`).set(body)
	
	const paymentRefResult = await fbApp.database().ref(`orders_refs/${body.paymentId}`).get()

	res.send(paymentRefResult)
})

app.get('/collections/:collection_id?', async (req, res) => {
	try {
		let response = {}

		if (req.params.collection_id) {
			let limit = 10
			let params = { limit }

			if (Object.keys(req.query).length) {
				const query = req.query;

				if (query.limit) {
					params.limit = parseInt(query.limit)
				}

				params = { ...params, ...query }

			}

			const collectionQuery = `{
				node(id: "gid://shopify/Collection/${req.params.collection_id}") {
					id
					... on Collection {
						products(first: ${params.limit}) {
							edges {
								cursor
								node {
									id
									title
									vendor
									options(first: 2) {
										name
										values
										position
									}
									featuredImage {
										id
										originalSrc
									}
									variants(first: 1) {
										edges {
											node {
												availableForSale
												price
												compareAtPrice
											}
										}
									}
								}
							}
							pageInfo {
								hasNextPage
								hasPreviousPage
							}
						}
					}
				}
			}
			`;

			const collection = await shopify.graphql(collectionQuery)
			const collection_products = collection.node.products;
			
			if (collection_products.pageInfo) {

				response.pagination = {
					next: false,
					prev: false
				}
				
				if (collection_products.pageInfo.hasNextPage) {
					response.pagination.next = collection_products.edges.pop().cursor
				}
		
				if (collection_products.pageInfo.hasPreviousPage) {
					response.pagination.prev = collection_products.edges[0].cursor
				}
			}

			response.products = collection_products.edges
	
		} else {
			const collectionListing = await shopify.smartCollection.list({ limit: 250, ids: '161088503878,33447051383,66735407174,21157904425,161018052678' })

			response = collectionListing
		}

		res.send(response);

	} catch (error) {

		console.error(error)

		res.send(error)
	}
})

getProductListing = async (params) => {
	try {
		const productListingsQuery = `{
			node(id: "gid://shopify/Channel/${shopifyConfig.salesChannel}") {
				id
				... on Channel {
					products(first: ${params.limit}) {
						edges {
							cursor
							node {
								id
								title
								vendor
								options(first: 2) {
									name
									values
									position
								}
								featuredImage {
									id
									originalSrc
								}
								variants(first: 1) {
									edges {
										node {
											availableForSale
											price
											compareAtPrice
										}
									}
								}
							}
						}
						pageInfo {
							hasNextPage
							hasPreviousPage
						}
					}
				}
			}
		}
		`;

		const collection = await shopify.graphql(productListingsQuery)
		const collection_products = collection.node.products;

		return collection_products.edges

	} catch (error) {
		console.error(error)
	}
}

getProductBy = async (id) => {
	try {
		return await shopify.productListing.get(id)
	} catch (error) {
		console.error('getProductBy  =>',error)
		return error
	}
}

app.get('/products/:id?', async (req, res) => {
	try {
		let limit = 10
		let params = { limit }
		const response = {}

		if (Object.keys(req.query).length) {
			const query = req.query;

			if (query.limit) {
				query.limit = parseInt(query.limit)
			}

			params = { ...params, ...query }

		}

		if (req.params.id) {

			const product = await getProductBy(parseInt(req.params.id))
			response.product = product;
		} else {

			const productListing = await getProductListing(params)

			response.products = productListing;
			response.pagination = {
				prev: productListing.previousPageParameters ? productListing.previousPageParameters['page_info'] : false,
				next: productListing.nextPageParameters ? productListing.nextPageParameters['page_info'] : false
			}

		}

		res.send(response);

	} catch (error) {

		res.send(error);
	}
})

app.get('/product/:id', async (req, res) => {

	try {

		const product = await getProductBy(parseInt(req.params.id))

		res.send(product);

	} catch (error) {

		console.error(error)

		res.send(error)
	}
})

getApplyToken = async (id) => {
	try {
		return await shopify.productListing.get(id)
	} catch (error) {
		return error
	}
}

app.listen(port, () => {
	console.log(`Vodacom Miniprogram listening at http://localhost:${port}`)
})
