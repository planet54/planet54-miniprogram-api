const fs = require('fs')
const alipayClient = require('../services/alipay-client-service')
const signatureService = require('../services/signature-serivce') 
const test = {
  applyAuthCode: async function () {

   const authCodeResponse = await alipayClient.applyAuthCode().then(res => res).catch(err => err)

    console.log(authCodeResponse);

  },

  getAccessToken: async function () {
    const getAccessTokenResponse = await alipayClient.getAccessToken().then(res => res.data).catch(err => err)
    console.log(getAccessTokenResponse)
  },

  signedToken: async function () {
    const applyTokenSigned = await alipayClient.applyTokenSigned().then(res => res).catch(error => error)
    console.log(applyTokenSigned)
  }, 
  verify: function() {
    const signature = 'ej32ayP6HNw8WKLnZh0V+vJTzplW0j10EN2AJty0BAg8b+4PYdsAJ8uLHUhGU9x1ywYQqRA/peBD8+1jzUTnX2eD9HTFiikDAhrYFhm/+NuNfspPnpWLIXq22aD1lbzi/QbpMBSxjHFWtmR67nhSgHDEtI1ORIfAGmv0TH5NqZT+GxGpWlb36Ao+x7Y2iAQtdVghJ8w32lnqT8/ZTGJWHM0/XJ8l+1xQhnJyv5TmIE/k4hFgSaB8d14LQ8/K5Z3AqYd72WDZ3+gdeGrraFaQ9DzekqkBaI+3KHtHzjgk40VTqZTXXrRjqNFsZViU0pSITAe6/8Uca7xZxcKU2q/7ug=='
    
    const data = 'POST /v2/authorizations/applyTokenSigned2021032698533992500711.2021-08-06T12:33:33.442+02:00.{"grantType":"AUTHORIZATION_CODE","authCode":"0000000001MfBVMS57Hz35Ip00181652"}'
    
    const pubkey = fs.readFileSync('../certs/alipay_merchant_public_key.pem', 'utf8')

    return signatureService.verify(signature, data, pubkey)

  }
}

test.signedToken()
