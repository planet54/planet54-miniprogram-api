const path = require('path')
require('dotenv').config({path: path.resolve(__dirname, '../.env')})

module.exports = {
  clientID : process.env.CLIENT_ID, 
  userID: process.env.USER_ID, 
  gatewayUrl: process.env.GATWAY_URL,
  applyAuthCodePath: '/authorizations/applyAuthCode', 
  applyTokenPath: '/authorizations/applyToken', 
  userDetailsPath: '/user/inquiryUserInfo',
  merchantPrivateKeyPath: '../certs/alipay_merchant_private_key.pem', 
  alipayPlusPublicKeyPath: '../certs/alipay_merchant_public_key.pem'
}
