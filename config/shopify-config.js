const path = require('path')
require('dotenv').config({path: path.resolve(__dirname, '../.env')})

module.exports = {
	shopName: process.env.SHOP_NAME,
	apiKey: process.env.SHOP_API_KEY,
	password: process.env.SHOP_PASSWORD,
	salesChannel: process.env.SHOP_SALES_CHANNEL
}
