const fs = require('fs')
const axios = require('axios')
const moment = require('moment-timezone')
const path = require('path')
const { DateTime } = require('luxon')

const alipayConfig = require('../config/alipay-config')
const signatureService = require('./signature-serivce')

const privateKey = fs.readFileSync(
  path.resolve(__dirname, '../certs/alipay_merchant_private_key.pem'),
  `utf-8`,
)

const createSignaturePayload = (path, body, requestTime) => {
  const URIpath = new URL(`${alipayConfig.gatewayUrl}/${path}`).pathname
  const unsignedContent = `POST ${URIpath}\n${alipayConfig.clientID}.${requestTime}.${JSON.stringify(body)}`

  return unsignedContent
}

const client = {
  async getAccessToken(authCode) {

    const requestTime = DateTime.local().toISO()
    const data = {
      "grantType": 'AUTHORIZATION_CODE',
      "authCode": authCode
    }
    const pathname = 'authorizations/applyToken'
    const signaturePayload = createSignaturePayload(pathname, data, requestTime)
    const signature = signatureService.sign(signaturePayload, privateKey);

    const accessTokenResponse = await axios({
      method: 'post',
      url: `${alipayConfig.gatewayUrl}/${pathname}`,
      headers: {
        'Content-Type': 'application/json',
        'client-id': alipayConfig.clientID,
        'request-time': requestTime,
        'signature': `algorithm=RSA256, keyVersion=1, signature=${signature}`
      },
      data: JSON.stringify(data)
    })

    return accessTokenResponse

  },
  async applyAuthCode() {
    const pathname = 'authorizations/applyAuthCode'
    const data = {
      "clientId": alipayConfig.clientID,
      "userId": alipayConfig.userID,
      "scopes": "NOTIFICATION_INBOX,USER_NICKNAME,USER_NAME,USER_AVATAR,USER_GENDER,USER_BIRTHDAY,USER_NATIONALITY,USER_CONTACTINFO"
    }
    const requestTime = DateTime.local().toISO()
    const signaturePayload = createSignaturePayload(pathname, data, requestTime)
    const signature = signatureService.sign(signaturePayload, privateKey);

    const configPost = {
      method: 'post',
      url: `${alipayConfig.gatewayUrl}/${pathname}`,
      headers: {
        'Content-Type': 'application/json',
        'client-id': alipayConfig.clientID,
        'request-time': requestTime,
        'signature': `algorithm=RSA256, keyVersion=1, signature=${signature}`
      },
      data: JSON.stringify(data)
    }

    const apiRes = await axios(configPost)

    return apiRes.data

  },
  async applyTokenSigned(authCode) {
    const pathname = 'authorizations/applyTokenSigned'
    const authCodeResponse = await this.applyAuthCode().then(res => res).catch(err => err)

    const data = {
      "grantType": 'AUTHORIZATION_CODE',
      "authCode": !authCode? authCodeResponse.authCode : authCode
    }

    const requestTime = DateTime.local().toISO()
    const signaturePayload = createSignaturePayload(pathname, data, requestTime)
    const signature = signatureService.sign(signaturePayload, privateKey)

    const configPost = {
      method: 'POST',
      url: `${alipayConfig.gatewayUrl}/${pathname}`,
      headers: {
        'Content-Type': 'application/json',
        'client-id': alipayConfig.clientID,
        'request-time': requestTime,
        'signature': `algorithm=RSA256, keyVersion=1, signature=${signature}`
      },
      data: data
    }

    const apiRes = await axios(configPost)

    return apiRes.data
  },
  async inquiryUserInfo(accessToken) {

    const pathname = 'customers/user/inquiryUserInfo'

    const data = {
      'authClientId': alipayConfig.clientID,
      'accessToken': accessToken
    }

    const requestTime = DateTime.local().toISO()
    const signaturePayload = createSignaturePayload(pathname, data, requestTime)
    const signature = signatureService.sign(signaturePayload, privateKey)

    const configPost = {
      method: 'post',
      url: `${alipayConfig.gatewayUrl}/${pathname}`,
      headers: {
        'Content-Type': 'application/json',
        'client-id': alipayConfig.clientID,
        'request-time': requestTime,
        'signature': `algorithm=RSA256, keyVersion=1, signature=${signature}`
      },
      data: JSON.stringify(data)
    }

    const apiRes = await axios(configPost)
  
    return apiRes.data;
  },
  async pay(order) {

    const cartItems = order.data
    const requestTime = DateTime.local().toISO()
    const expiryTimestamp = () => moment().tz('Africa/Johannesburg').add(15, 'minutes').format('YYYY-MM-DDTHH:mm:ss.sss+02:00')
    const paymentNotificationURL = 'https://planet54-miniprogram-prod.herokuapp.com/orders/status'
    const payObject = {
      productCode: 'CASHIER_PAYMENT',
      salesCode: '51051000101000000011',
      paymentNotifyUrl: paymentNotificationURL,
      paymentRequestId: new Date().getUTCMilliseconds(),
      paymentRedirectUrl: paymentNotificationURL,
      paymentExpiryTime: expiryTimestamp(),
      paymentAmount: {
        currency: 'ZAR',
        value: cartItems.total_price
      },
      order: {
        line_items: cartItems.items,
        env: {
          terminalType: 'MINI_APP'
        },
        orderDescription: 'Vodapay Planet54 Order',
        buyer: {
          referenceBuyerId: order.customerId
        }
      }
    }

    const pathname = 'payments/pay'
    const signaturePayload = createSignaturePayload(pathname, payObject, requestTime)
    const signature = signatureService.sign(signaturePayload, privateKey)

    const configPost = {
      method: 'post',
      url: `${alipayConfig.gatewayUrl}/${pathname}`,
      headers: {
        'Content-Type': 'application/json',
        'client-id': alipayConfig.clientID,
        'request-time': requestTime,
        'signature': `algorithm=RSA256, keyVersion=1, signature=${signature}`
      },
      data: JSON.stringify(payObject)
    }

    const apiRes = await axios(configPost)

    return apiRes.data
  }
}

module.exports = client
