const crypto = require('crypto')

const sign = function (signaturePayload, privateKey) {
  const privateKeyObject = crypto.createPrivateKey(privateKey , 'utf8')
  const authoriser  = crypto.createSign('RSA-SHA256');

  authoriser.write(signaturePayload)
  authoriser.end()

  return authoriser.sign(privateKeyObject, 'base64')
}

const verify = function (signature, payload, publicKey) {
  const publicKeyObject = crypto.createPublicKey(publicKey , 'utf8')
  const auditor = crypto.createVerify('RSA-SHA256'); 
  
  auditor.write(payload)
  auditor.end()

  return !!(auditor.verify(publicKeyObject, signature, 'base64'))
}

module.exports = {
  sign,
  verify
}
